export const timeConvert=(time:any)=>{

    const date = new Date(time);
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const hour = date.getHours();
    const minute = date.getMinutes();

    return `${year}/${month}/${day} ${hour}:${minute}`;
}
export const openUrl=((url:string)=>{
    window.open(url)
})

export const sUrl=(url: string | number)=>{
    const baseUrl=localStorage.getItem('baseUrl')
    if(!baseUrl){
        return ''
    }
    return baseUrl+url
}
//axios 设置通用的headers参数
import { Gitlab } from '@gitbeaker/rest';
import axios from 'axios';
const api = new Gitlab({
    host:localStorage.getItem('baseUrl'),
    token: localStorage.getItem('token'),
});
const token=localStorage.getItem('token')
// 创建 axios 实例
const instance = axios.create({
  baseURL: localStorage.getItem('baseUrl'), // 设置默认的 baseURL，这样在请求时就不必每次都写完整的 URL
  headers: {
    'Content-Type': 'application/json', // 设置默认的 headers
  },
});

// 添加请求拦截器
instance.interceptors.request.use(
  (config: any) => {
    // 在发送请求之前可以在这里添加一些通用的 headers 参数
    config.headers['PRIVATE-TOKEN'] =`${token}`; // 假设我们有一个名为 your_token 的变量，它存储了我们的认证 token
    return config;
  },
  (error: any) => {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);

// 添加响应拦截器
instance.interceptors.response.use(
  (response: any) => {
    // 对响应数据做些什么
      if(response.status===200){
          return response.data
      }else{
          return [];
      }
  },
  (error: any) => {
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);


export function getMergeRequests(options: any){
    return instance.get('api/v4/merge_requests?scope=all',{
        params:{
            ...options
        }
    });
}


export function getProjects(){
    return instance.get('api/v4/projects',{params:{per_page:30,order_by:'updated_at',sort: 'desc',membership:true}});
}


export function getEvents(params: any){
    return api.Events.all({...params})
}
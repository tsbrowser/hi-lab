import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/reset.css';
import './assets/ttcss/index.css'
createApp(App).use(Antd).mount('#app')

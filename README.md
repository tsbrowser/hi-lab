## Hilab  基于GitlabAPI的天天工作台小卡片


### 功能

- 支持快捷访问，可选取项目添加
- 支持展示合并请求并快速打开去处理

![图片](https://jxxt-1257689580.cos.ap-chengdu.myqcloud.com/2Xhe-Yo53-fk6Z-deAo)

可直接下载dist下的打包文件，在天天工作台安装使用。

也可以直接修改内部的文件作为示例。

开发教程见：

[文档站开发教程](http://localhost:8080/ttdoc/ttapp/l1.html)